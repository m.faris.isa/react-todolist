import React, { useState } from 'react';

import Paper from '../Components/Paper/Paper';
import Header from '../Components/Header/Header';
import ToDoForm from '../Components/Todoform/Todoform';
import ToDos from '../Components/Todos/Todos';

const TodoList = () =>  {
  const [todos, setTodos ] = useState([
    { text: "Learning React!", isCompleted: false },
    { text: "Learning React Hooks!", isCompleted: false },
    { text: "Learning styling in React!", isCompleted: false }
  ]);
  const [showAdd, setShowAdd] = useState(false);

  const addTodo = value => {

    if (todos.length < 10 ){
      const addedTodo = [...todos, { text: value, isCompleted: false}];
      setTodos(addedTodo);
    } else {
      alert("YOU CANT PUT MORE THAN 10 LIST")
    }

  };

  const completeTodo = (index) => {
    const addedTodo = [...todos];
    addedTodo[index].isCompleted = !addedTodo[index].isCompleted;

    setTodos(addedTodo);
  };

  const clearTodos = () => !showAdd && setTodos([])
  
  const showAddToggle = () => setShowAdd(!showAdd);

  return (
    <Paper>
        <Header showAddToggle={showAddToggle} showAdd={showAdd} clearTodos={clearTodos}/>
        <ToDoForm addTodo={addTodo} showAdd={showAdd}/>
        <ToDos todos={todos} completeTodo={completeTodo}/>
    </Paper>
  );

}

export default TodoList;
