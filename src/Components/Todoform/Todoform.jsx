import React, { useState } from "react";
import PropTypes from "prop-types";
import { useTheme } from "emotion-theming";

// import styles from "./Todoform.module.css";
import * as styles from "./Todoform.style";

const TodoForm = ({addTodo, showAdd}) => {
    const [ value, setValue ] = useState("");
    const theme = useTheme();

    const handleFormSubmit = e => {
        e.preventDefault();

        if(!value) {
            alert("NO BLANK TO DO !");
            return;
        };

        if (value.length > 40 ){
            alert ("CREATE A SHORTER TO DO TEXT !");
            setValue("");
            return;
        }

        addTodo(value);
        setValue("");
    };

    if (showAdd){
        return (
        <section css={styles.add}>
            <form css={styles.addForm} onSubmit={handleFormSubmit}>
                <input type="text" css={styles.addInput({ theme })} value={value} 
                onChange={ e => setValue(e.target.value)} />
                <button css={styles.addBtn({ theme })}>Add</button>
            </form>
        </section>
        );
    } else {
        return null;
    }
};

TodoForm.propTypes = {
    addTodo: PropTypes.func.isRequired,
    showAdd: PropTypes.bool.isRequired
}

export default TodoForm;