import React from "react";
import PropTypes from "prop-types";
import { useTheme } from "emotion-theming";

import Button from "../Button/Button";
// import styles from "./header.module.css";
import * as styles from "./Header.styles";

const Header = ({ showAddToggle, showAdd, clearTodos }) => {
    const theme = useTheme();

    return (
        
        <section css={styles.header}>
            <Button text={showAdd ? 'Finish' : 'Add'} onClick={showAddToggle}/>
            
            <h1 css={styles.headerTitle(theme)}>ToDo List</h1>
            
            <Button text="Clear" onClick={clearTodos} color="red" align="right"/>

        </section>
    );
};

Header.propTypes = {
    showAddToggle: PropTypes.func.isRequired,
    showAdd: PropTypes.bool.isRequired,
    clearTodos: PropTypes.func.isRequired
};

export default Header;