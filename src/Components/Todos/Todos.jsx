import React from "react";
import PropTypes from "prop-types";

import Todo from "../Todo/Todo";

//import styles from "./Todos.module.css"
import * as styles from "./Todos.styles";

const Todos = ({ todos, completeTodo }) => {
    return (
        <section css={styles.todos}>
            {todos.length > 0 && 
                todos.map((todo, index) => {
                    return (
                        <Todo 
                            key={index} 
                            text={todo.text} 
                            isCompleted={todo.isCompleted} 
                            completeTodo={completeTodo} 
                            index={index}
                        />
                    );
                })}
                {todos.length === 0 && (
                    <div css={styles.todosPlaceholderText}>
                        Add to do by clicking <span css={styles.addButtonPlaceholderText}>add</span> button on the top of left corner
                    </div>
                )}
        </section>
    
    );
};

Todos.propTypes = {
    todos: PropTypes.arrayOf(
        PropTypes.shape({
            text: PropTypes.string
        })
    ),
    completeTodo: PropTypes.func.isRequired
};

export default Todos;