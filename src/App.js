import React from 'react';
// import './App.css';
import './Style.css';
import { ThemeProvider } from "emotion-theming";

import TodoList from './Pages/TodoList';

const theme = {
  color: {
    primary: {
      black: "#484848",
      red: "#E06262",
    }
  },
  background: {
    color: {
      primary: "#f2eecb",
    }
  }
}

function App() {
  return (
    <ThemeProvider theme={theme}>
      <TodoList />
    </ThemeProvider>
  );
}

export default App;
